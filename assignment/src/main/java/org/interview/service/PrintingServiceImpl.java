package org.interview.service;

import java.io.PrintStream;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.interview.entity.Author;
import org.interview.entity.Message;
import org.interview.resources.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrintingServiceImpl implements PrintingService {

	protected static final String NEW_LINE = "\r\n";

	private final Language language;

	@Autowired
	public PrintingServiceImpl(Language language) {
		this.language = language;
	}

	@Override
	public void printTwitterMessages(
			TreeMap<Author, TreeSet<Message>> messagesByAuthor,
			PrintStream printStream) {

		if (messagesByAuthor.isEmpty()) {
			printStream.print(NEW_LINE + language.noMessages() + NEW_LINE);
			return;
		}
		printStream.print(NEW_LINE + language.messagesByAuthors() + NEW_LINE);

		for (Entry<Author, TreeSet<Message>> entry : messagesByAuthor
				.entrySet()) {

			Author author = entry.getKey();
			TreeSet<Message> messages = entry.getValue();

			printStream.print(NEW_LINE + author + NEW_LINE);

			for (Message message : messages) {
				printStream.print(message + NEW_LINE);
			}
		}

	}

	@Override
	public void printHelp(Options options) {

		String header = language.helpHeader();
		String footer = language.helpFooter();
		String appName = language.appName();

		System.out.println(NEW_LINE);
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp(appName, header, options, footer, true);
	}

}
