package org.interview.parsing;

import java.text.ParseException;

import org.interview.entity.Message;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Component
public class MessageJsonParser extends BaseJsonParser {

	public Message parse(String json) throws ParseException {
		JsonParser parser = new JsonParser();
		JsonObject messageJsonObject = parser.parse(json).getAsJsonObject();
		JsonObject authorJsonObject = messageJsonObject.getAsJsonObject("user");
		Message message = Message
				.newBuilder()
				.withAuthorId(stringify(authorJsonObject.get("id")))
				.withCreationDate(parseToMilliseconds(
						messageJsonObject.get("created_at")))
				.withMessageId(stringify(messageJsonObject.get("id")))
				.withText(stringify(messageJsonObject.get("text")))
				.build();
		return message;
	}

}
