package org.interview.exception;

public class InvalidCommandLineParameterException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidCommandLineParameterException(String message) {
		super(message);
	}

}
