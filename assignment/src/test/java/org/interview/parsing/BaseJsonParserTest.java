package org.interview.parsing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.io.IOException;

import org.junit.Test;
import org.mockito.Mockito;

import com.google.gson.JsonElement;

public class BaseJsonParserTest {

	private BaseJsonParser baseJsonParser = new BaseJsonParser() {
	};

	@Test
	public void shouldExtractStringValueWhenJsonElementNotNull()
			throws IOException {
		JsonElement jsonElementMock = Mockito.mock(JsonElement.class);
		String expected = "I am great json element.";
		doReturn(expected).when(jsonElementMock).getAsString();

		String result = baseJsonParser.stringify(jsonElementMock);

		assertEquals(expected, result);
	}

	@Test
	public void shouldReturnEmptyStringWhenJsonElementIsNull()
			throws IOException {
		JsonElement nullJsonElement = null;

		String result = baseJsonParser.stringify(nullJsonElement);

		assertEquals("", result);
	}

}
