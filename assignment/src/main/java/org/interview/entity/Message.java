package org.interview.entity;

public class Message implements Comparable<Message> {

	private String messageId;
	private long creationDate;
	private String text;
	private String authorId;

	public Message(Builder builder) {
		this.messageId = builder.messageId;
		this.creationDate = builder.creationDate;
		this.text = builder.text;
		this.authorId = builder.authorId;
	}

	public static Builder newBuilder() {
		return new Builder();
	}

	public String getMessageId() {
		return messageId;
	}

	public long getCreationDate() {
		return creationDate;
	}

	public String getText() {
		return text;
	}

	public String getAuthorId() {
		return authorId;
	}

	public static class Builder {

		private String messageId;
		private long creationDate;
		private String text;
		private String authorId;

		public Builder withMessageId(String messageId) {
			this.messageId = messageId;
			return this;
		}

		public Builder withCreationDate(long creationDate) {
			this.creationDate = creationDate;
			return this;
		}

		public Builder withText(String text) {
			this.text = text;
			return this;
		}

		public Builder withAuthorId(String authorId) {
			this.authorId = authorId;
			return this;
		}

		public Message build() {
			return new Message(this);
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + (int) (creationDate ^ (creationDate >>> 32));
		result = prime * result
				+ ((messageId == null) ? 0 : messageId.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (creationDate != other.creationDate)
			return false;
		if (messageId == null) {
			if (other.messageId != null)
				return false;
		} else if (!messageId.equals(other.messageId))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "\n    messageId    : " + messageId + ",\n    creationDate : "
				+ creationDate + ",\n    text         : \"" + text
				+ "\",\n    authorId     : " + authorId;
	}

	@Override
	public int compareTo(Message author) {
		if (this.creationDate != author.creationDate) {
			return Long.compare(this.creationDate, author.creationDate);
		}
		if (!this.authorId.equals(author.authorId)) {
			return this.authorId.compareTo(author.authorId);
		}
		if (!this.messageId.equals(author.messageId)) {
			return this.messageId.compareTo(author.messageId);
		}
		if (!this.text.equals(author.text)) {
			return this.text.compareTo(author.text);
		}
		return 0;
	}

}
