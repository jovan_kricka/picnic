package org.interview.cmd;

import org.apache.commons.cli.Option;

public class NumericOptionWrapper {

	private Option option;
	private long defaultValue;
	private String errorMessage;

	public NumericOptionWrapper(Builder builder) {
		this.option = builder.option;
		this.defaultValue = builder.defaultValue;
		this.errorMessage = builder.errorMessage;
	}

	public Option getOption() {
		return option;
	}

	public long getDefaultValue() {
		return defaultValue;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public static Builder newBuilder() {
		return new Builder();
	}

	public static class Builder {

		private Option option;
		private long defaultValue;
		private String errorMessage;

		public Builder withOption(Option option) {
			this.option = option;
			return this;
		}

		public Builder withDefaultValue(long defaultValue) {
			this.defaultValue = defaultValue;
			return this;
		}

		public Builder withErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
			return this;
		}

		public NumericOptionWrapper build() {
			return new NumericOptionWrapper(this);
		}

	}

}
