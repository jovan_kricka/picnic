package org.interview.resources;

import javax.annotation.Resource;

import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource("language.properties")
public class Language {

	@Resource
	private Environment env;

	public String fetchingStarted() {
		return env.getRequiredProperty("fetchingStarted");
	}

	public String progress() {
		return env.getRequiredProperty("progress");
	}

	public String fetchingFinished() {
		return env.getRequiredProperty("fetchingFinished");
	}

	public String messagesByAuthors() {
		return env.getRequiredProperty("messagesByAuthors");
	}

	public String noMessages() {
		return env.getRequiredProperty("noMessages");
	}

	public String helpHeader() {
		return env.getRequiredProperty("helpHeader");
	}

	public String helpFooter() {
		return env.getRequiredProperty("helpFooter");
	}

	public String appName() {
		return env.getRequiredProperty("appName");
	}

	public String error() {
		return env.getRequiredProperty("error");
	}

}
