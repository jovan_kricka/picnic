package org.interview.parsing;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonElement;

public abstract class BaseJsonParser {

	protected String stringify(JsonElement field) {
		return field == null ? "" : field.getAsString();
	}

	protected long parseToMilliseconds(JsonElement dateField)
			throws ParseException {
		String dateString = stringify(dateField);
		if (dateString.isEmpty()) {
			return 0;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(
				"EEE MMM dd HH:mm:ss ZZZZZ yyyy");
		Date date = formatter.parse(dateString);
		return date.getTime();
	}

}
