package picnic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScreeningTest {

	public static void main(String[] args) {
		List<Integer> data1 = Arrays.asList(1, 4, 7);
		List<Integer> data2 = Arrays.asList(123, -2, 477, 3, 14, 6551);

		ScreeningTest obj = new ScreeningTest();

		int result = obj.fold(data1);
		System.out.println(result);

		int yourAnswer = obj.fold(data2); // what is the answer for this one???
		System.out.println(yourAnswer);
	}

	private int fold(List<Integer> input) {
		if (input.isEmpty()) {
			return 0;
		}
		int firstElement = input.get(0);
		if (input.size() == 1) {
			return firstElement;
		}
		List<Integer> resultList = new ArrayList<>(input.size() - 1);
		for (int i = 1; i < input.size(); i++) {
			resultList.add(input.get(i) + firstElement);
		}
		return fold(resultList);
	}

}
