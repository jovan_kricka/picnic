package org.interview.entity;

public class Author implements Comparable<Author> {

	private String userId;
	private long creationDate;
	private String name;
	private String screenName;

	public Author(Builder builder) {
		this.userId = builder.userId;
		this.creationDate = builder.creationDate;
		this.name = builder.name;
		this.screenName = builder.screenName;
	}

	public static Builder newBuilder() {
		return new Builder();
	}

	public String getUserId() {
		return userId;
	}

	public long getCreationDate() {
		return creationDate;
	}

	public String getName() {
		return name;
	}

	public String getScreenName() {
		return screenName;
	}

	public static class Builder {

		private String userId = "";
		private long creationDate;
		private String name = "";
		private String screenName = "";

		public Builder withUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public Builder withCreationDate(long creationDate) {
			this.creationDate = creationDate;
			return this;
		}

		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public Builder withScreenName(String screenName) {
			this.screenName = screenName;
			return this;
		}

		public Author build() {
			return new Author(this);
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (creationDate ^ (creationDate >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((screenName == null) ? 0 : screenName.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (creationDate != other.creationDate)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (screenName == null) {
			if (other.screenName != null)
				return false;
		} else if (!screenName.equals(other.screenName))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Author\n" + "\n userId       : " + userId + ","
				+ "\n creationDate : " + creationDate + ","
				+ "\n name         : " + name + "," + "\n screenName   : "
				+ screenName;
	}

	@Override
	public int compareTo(Author author) {
		if (this.creationDate != author.creationDate) {
			return Long.compare(this.creationDate, author.creationDate);
		}
		if (!this.name.equals(author.name)) {
			return this.name.compareTo(author.name);
		}
		if (!this.screenName.equals(author.screenName)) {
			return this.screenName.compareTo(author.screenName);
		}
		if (!this.userId.equals(author.userId)) {
			return this.userId.compareTo(author.userId);
		}
		return 0;
	}

}
