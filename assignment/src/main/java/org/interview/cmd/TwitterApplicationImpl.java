package org.interview.cmd;

import java.io.IOException;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.interview.entity.Author;
import org.interview.entity.Message;
import org.interview.exception.InvalidCommandLineParameterException;
import org.interview.exception.TwitterAuthenticationException;
import org.interview.service.PrintingService;
import org.interview.service.TwitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TwitterApplicationImpl implements TwitterApplication {

	public static final String HELP_OPTION_DESCRIPTION = "information on how to use the tool";
	public static final String HELP_LONG_OPTION = "help";
	public static final String HELP_SHORT_OPTION = "h";

	public static final String MESSAGES_LIMIT_OPTION_DESCRIPTION = "maximum tweets to download";
	public static final String MESSAGES_LIMIT_LONG_OPTION = "messagesLimit";
	public static final String MESSAGES_LIMIT_SHORT_OPTION = "m";
	public static final String INVALID_MESSAGE_LIMIT = "Provided value for message limit is not a valid number.";

	public static final String STREAMING_PERIOD_OPTION_DESCRIPTION = "maximum time period (in millis) for tweets downloading";
	public static final String STREAMING_PERIOD_LONG_OPTION = "streamingPeriod";
	public static final String STREAMING_PERIOD_SHORT_OPTION = "s";
	public static final String INVALID_STREAMING_PERIOD = "Provided value for streaming period is not a valid number.";

	private final Option HELP_OPTION = Option
			.builder(HELP_SHORT_OPTION)
			.argName(HELP_LONG_OPTION)
			.longOpt(HELP_LONG_OPTION)
			.desc(HELP_OPTION_DESCRIPTION)
			.build();
	private final NumericOptionWrapper MESSAGES_LIMIT_WRAPPER = NumericOptionWrapper
			.newBuilder()
			.withOption(Option
					.builder(MESSAGES_LIMIT_SHORT_OPTION)
					.argName(MESSAGES_LIMIT_LONG_OPTION)
					.hasArg()
					.longOpt(MESSAGES_LIMIT_LONG_OPTION)
					.desc(MESSAGES_LIMIT_OPTION_DESCRIPTION)
					.build())
			.withDefaultValue(TwitterService.DEFAULT_MESSAGES_LIMIT)
			.withErrorMessage(INVALID_MESSAGE_LIMIT)
			.build();
	private final NumericOptionWrapper STREAMING_PERIOD_WRAPPER = NumericOptionWrapper
			.newBuilder()
			.withOption(Option
					.builder(STREAMING_PERIOD_SHORT_OPTION)
					.argName(STREAMING_PERIOD_LONG_OPTION)
					.hasArg()
					.longOpt(STREAMING_PERIOD_LONG_OPTION)
					.desc(STREAMING_PERIOD_OPTION_DESCRIPTION)
					.build())
			.withDefaultValue(TwitterService.DEFAULT_STREAMING_PERIOD)
			.withErrorMessage(INVALID_STREAMING_PERIOD)
			.build();

	private final TwitterService twitterService;
	private final PrintingService printingService;

	@Autowired
	public TwitterApplicationImpl(TwitterService twitterService,
			PrintingService printingService) {
		this.twitterService = twitterService;
		this.printingService = printingService;
	}

	@Override
	public void run(String[] args, String track)
			throws TwitterAuthenticationException, IOException, ParseException,
			InvalidCommandLineParameterException, java.text.ParseException {
		Options options = getOptions();
		CommandLineParser parser = new DefaultParser();
		CommandLine cmd = parser.parse(options, args);

		if (hasHelpOption(cmd)) {
			printingService.printHelp(options);
		} else {
			long messagesLimit = getLongNumberCmdLineOption(cmd,
					MESSAGES_LIMIT_WRAPPER);
			long streamingPeriod = getLongNumberCmdLineOption(cmd,
					STREAMING_PERIOD_WRAPPER);
			twitterService.setStreamingOptions(streamingPeriod, messagesLimit);
			fetchAndPrintMessages(track);
		}
	}

	private boolean hasHelpOption(CommandLine cmd) {
		return cmd.hasOption(HELP_SHORT_OPTION);
	}

	void fetchAndPrintMessages(String track)
			throws TwitterAuthenticationException, IOException,
			java.text.ParseException {
		TreeMap<Author, TreeSet<Message>> messages = twitterService
				.retrieveMessages(track, System.out);
		printingService.printTwitterMessages(messages, System.out);

	}

	private long getLongNumberCmdLineOption(CommandLine cmd,
			NumericOptionWrapper wrapper)
			throws InvalidCommandLineParameterException {
		if (cmd.hasOption(wrapper.getOption().getArgName())) {
			String optionValue = cmd
					.getOptionValue(wrapper.getOption().getArgName());
			try {
				return Long.parseLong(optionValue);
			} catch (NumberFormatException nfE) {
				throw new InvalidCommandLineParameterException(
						wrapper.getErrorMessage());
			}
		} else {
			return wrapper.getDefaultValue();
		}
	}

	private Options getOptions() {
		Options options = new Options();
		options.addOption(HELP_OPTION);
		options.addOption(MESSAGES_LIMIT_WRAPPER.getOption());
		options.addOption(STREAMING_PERIOD_WRAPPER.getOption());
		return options;
	}

}
