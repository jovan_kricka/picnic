package org.interview;

import static org.mockito.Matchers.any;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;

import com.google.api.client.http.GenericUrl;

public abstract class BaseTest {

	protected static final String MESSAGE_JSON = "message.json";
	protected static final String MESSAGE_STREAM_JSON = "message_stream.json";
	protected static final String BIEBER = "bieber";

	protected static final long AUTHOR_CREATION_DATE = 1491629959000l;
	protected static final String AUTHOR_NAME = "Jay DESPACITO";
	protected static final String AUTHOR_SCREEN_NAME = "drewvattor";
	protected static final String AUTHOR_ID = "850583816582332417";

	protected String getContentOfResourceFile(String pathname)
			throws IOException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(pathname).getFile());
		String content = IOUtils.toString(new FileInputStream(file), "UTF-8");
		return content;
	}

	protected GenericUrl anyGenericUrl() {
		return any(GenericUrl.class);
	}

}
