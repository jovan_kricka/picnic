package org.interview.service;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.TreeMap;
import java.util.TreeSet;

import org.interview.BaseTest;
import org.interview.app.AppContext;
import org.interview.entity.Author;
import org.interview.entity.Message;
import org.interview.resources.Language;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.google.api.client.util.Sets;
import com.google.common.collect.Maps;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppContext.class, loader = AnnotationConfigContextLoader.class)
public class PrintingServiceImplTest extends BaseTest {

	private static final String EXPECTED_OUTPUT_FOR_SINGLE_MESSAGE = "\r\nHere are the tweets grouped by their authors:\r\n\r\nAuthor\n\n userId       : 1,"
			+ "\n creationDate : 1,\n name         : John Doe,\n screenName   : johndoe\r\n\n    "
			+ "messageId    : 1,\n    creationDate : 1,\n    text         : \"Justin Bieber is"
			+ " the best\",\n    authorId     : 1\r\n";

	@Autowired
	private PrintingServiceImpl printingService;
	@Autowired
	private Language language;

	@Test
	public void shouldPrintCorrectInfoWhenThereAreNoMessages() {
		TreeMap<Author, TreeSet<Message>> messagesByAuthor = Maps.newTreeMap();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(byteArrayOutputStream);

		printingService.printTwitterMessages(messagesByAuthor, printStream);

		String printedContent = new String(byteArrayOutputStream.toByteArray(),
				StandardCharsets.UTF_8);
		String expectedContent = PrintingServiceImpl.NEW_LINE
				+ language.noMessages() + PrintingServiceImpl.NEW_LINE;
		assertEquals(expectedContent, printedContent);
	}

	@Test
	public void shouldPrintAuthorAndMessageWhenThereIsOneMessage() {
		TreeMap<Author, TreeSet<Message>> messagesByAuthor = buildMessagesByAuthor();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(byteArrayOutputStream);

		printingService.printTwitterMessages(messagesByAuthor, printStream);

		String printedContent = new String(byteArrayOutputStream.toByteArray(),
				StandardCharsets.UTF_8);
		String expectedContent = EXPECTED_OUTPUT_FOR_SINGLE_MESSAGE;
		assertEquals(expectedContent, printedContent);
	}

	private TreeMap<Author, TreeSet<Message>> buildMessagesByAuthor() {
		Author author = Author
				.newBuilder()
				.withCreationDate(1)
				.withName("John Doe")
				.withScreenName("johndoe")
				.withUserId("1")
				.build();
		Message message = Message
				.newBuilder()
				.withAuthorId("1")
				.withCreationDate(1)
				.withMessageId("1")
				.withText("Justin Bieber is the best")
				.build();

		TreeMap<Author, TreeSet<Message>> messagesByAuthor = Maps.newTreeMap();
		TreeSet<Message> messages = Sets.newTreeSet();
		messages.add(message);
		messagesByAuthor.put(author, messages);
		return messagesByAuthor;
	}

}
