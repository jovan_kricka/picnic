package org.interview.cmd;

import java.io.IOException;

import org.apache.commons.cli.ParseException;
import org.interview.exception.InvalidCommandLineParameterException;
import org.interview.exception.TwitterAuthenticationException;

/**
 * Class that executes our application based on command line parameters.
 */
public interface TwitterApplication {

	/**
	 * Executes application based on command line parameters.
	 * 
	 * @param args
	 *            command line parameters
	 * @param track
	 *            track of the messages
	 * @throws IOException
	 * @throws TwitterAuthenticationException
	 * @throws ParseException
	 * @throws InvalidCommandLineParameterException
	 * @throws java.text.ParseException
	 */
	void run(String[] args, String track) throws TwitterAuthenticationException,
			IOException, ParseException, InvalidCommandLineParameterException, java.text.ParseException;

}
