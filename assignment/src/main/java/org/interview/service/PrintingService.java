package org.interview.service;

import java.io.PrintStream;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.cli.Options;
import org.interview.entity.Author;
import org.interview.entity.Message;

/**
 * Provides interface for printing relevant information to the output stream.
 *
 */
public interface PrintingService {

	/**
	 * Prints twitter messages grouped by author to the output stream
	 * 
	 * @param messagesByAuthor
	 *            twitter messages grouped by user
	 * @param printStream
	 *            output stream to print messages to
	 */
	void printTwitterMessages(
			TreeMap<Author, TreeSet<Message>> messagesByAuthor,
			PrintStream printStream);

	/**
	 * Prints help information about the application to System.out.
	 * 
	 * @param options
	 *            application command line options
	 */
	void printHelp(Options options);

}
