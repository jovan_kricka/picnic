package org.interview.app;

import java.io.IOException;

import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;
import org.interview.cmd.TwitterApplication;
import org.interview.exception.InvalidCommandLineParameterException;
import org.interview.exception.TwitterAuthenticationException;
import org.interview.resources.Language;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class App {

	private static final String TRACKING_STRING = "bieber";

	private static ApplicationContext applicationContext;
	final static Logger logger = Logger.getLogger(App.class);

	public static void main(String[] args) {

		initializeSpringContext();

		TwitterApplication twitterApplication = getTwitterApplication();
		Language language = getLanguage();
		try {
			twitterApplication.run(args, TRACKING_STRING);
		} catch (ParseException | InvalidCommandLineParameterException ex) {
			logger.info(ex);
			System.out.println(ex.getMessage());
		} catch (TwitterAuthenticationException twAEx) {
			logger.info(twAEx);
			System.out.println(language.error());
		} catch (IOException ioEx) {
			logger.info(ioEx);
			System.out.println(language.error());
		} catch (java.text.ParseException pEx) {
			logger.info(pEx);
			System.out.println(language.error());
		}
	}

	private static void initializeSpringContext() {
		applicationContext = new AnnotationConfigApplicationContext(
				AppContext.class);
	}

	private static TwitterApplication getTwitterApplication() {
		return applicationContext.getBean(TwitterApplication.class);
	}

	private static Language getLanguage() {
		return applicationContext.getBean(Language.class);
	}

}
