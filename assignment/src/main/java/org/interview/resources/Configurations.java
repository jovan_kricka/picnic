package org.interview.resources;

import javax.annotation.Resource;

import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@PropertySource("configuration.properties")
public class Configurations {

	@Resource
	private Environment env;

	public String consumerKey() {
		return env.getRequiredProperty("consumerKey");
	}

	public String consumerSecret() {
		return env.getRequiredProperty("consumerSecret");
	}

}
