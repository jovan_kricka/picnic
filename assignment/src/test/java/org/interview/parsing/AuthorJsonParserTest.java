package org.interview.parsing;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;

import org.interview.BaseTest;
import org.interview.app.AppContext;
import org.interview.entity.Author;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppContext.class, loader = AnnotationConfigContextLoader.class)
public class AuthorJsonParserTest extends BaseTest {

	@Autowired
	private AuthorJsonParser authorJsonParser;

	@Test
	public void shouldCorrectlyParseAuthorFieldsFromJson()
			throws IOException, ParseException {
		String messageJson = getContentOfResourceFile(MESSAGE_JSON);

		Author author = authorJsonParser.parse(messageJson);

		assertEquals(author.getUserId(), AUTHOR_ID);
		assertEquals(author.getCreationDate(), AUTHOR_CREATION_DATE);
		assertEquals(author.getScreenName(), AUTHOR_SCREEN_NAME);
		assertEquals(author.getName(), AUTHOR_NAME);
	}

}
