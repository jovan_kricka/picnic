package org.interview.cmd;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.PrintStream;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.interview.BaseTest;
import org.interview.app.AppContext;
import org.interview.entity.Author;
import org.interview.entity.Message;
import org.interview.exception.InvalidCommandLineParameterException;
import org.interview.exception.TwitterAuthenticationException;
import org.interview.service.PrintingService;
import org.interview.service.TwitterService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.google.common.collect.Maps;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppContext.class, loader = AnnotationConfigContextLoader.class)
public class TwitterApplicationImplTest extends BaseTest {

	@Mock
	private TwitterService twitterService;
	@Mock
	private PrintingService printingService;

	private TwitterApplicationImpl twitterApplication;

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		twitterApplication = new TwitterApplicationImpl(twitterService,
				printingService);
	}

	@Test
	public void shouldRetrieveTwitterMessagesAndPrintThem()
			throws TwitterAuthenticationException, IOException,
			java.text.ParseException {

		TreeMap<Author, TreeSet<Message>> dummyMessagesByAuthor = Maps
				.newTreeMap();
		doReturn(dummyMessagesByAuthor).when(twitterService).retrieveMessages(
				anyString(), anyPrintStream());
		doNothing().when(printingService).printTwitterMessages(
				anyMessagesByAuthor(), anyPrintStream());

		twitterApplication.fetchAndPrintMessages(BIEBER);

		verify(twitterService).retrieveMessages(anyString(), anyPrintStream());
		verify(printingService).printTwitterMessages(anyMessagesByAuthor(),
				anyPrintStream());
	}

	@Test
	public void shouldOnlyPrintHelpWhenHelpShortOptionProvided()
			throws TwitterAuthenticationException, IOException, ParseException,
			InvalidCommandLineParameterException, java.text.ParseException {

		String[] args = { "-" + TwitterApplicationImpl.HELP_SHORT_OPTION };

		twitterApplication.run(args, BIEBER);

		verify(twitterService, times(0)).retrieveMessages(anyString(),
				anyPrintStream());
		verify(printingService, times(0))
				.printTwitterMessages(anyMessagesByAuthor(), anyPrintStream());
		verify(printingService).printHelp(anyOptions());
	}

	@Test
	public void shouldOnlyPrintHelpWhenHelpLongOptionProvided()
			throws TwitterAuthenticationException, IOException, ParseException,
			InvalidCommandLineParameterException, java.text.ParseException {

		String[] args = { "-" + TwitterApplicationImpl.HELP_LONG_OPTION };

		twitterApplication.run(args, BIEBER);

		verify(twitterService, times(0)).retrieveMessages(anyString(),
				anyPrintStream());
		verify(printingService, times(0))
				.printTwitterMessages(anyMessagesByAuthor(), anyPrintStream());
		verify(printingService).printHelp(anyOptions());
	}

	@Test
	public void shouldRetrieveTwitterMessagesAndPrintThemWithMessagesLimitOption()
			throws TwitterAuthenticationException, IOException, ParseException,
			InvalidCommandLineParameterException, java.text.ParseException {

		TreeMap<Author, TreeSet<Message>> dummyMessagesByAuthor = Maps
				.newTreeMap();
		doReturn(dummyMessagesByAuthor).when(twitterService).retrieveMessages(
				anyString(), anyPrintStream());
		doNothing().when(printingService).printTwitterMessages(
				anyMessagesByAuthor(), anyPrintStream());
		doNothing().when(twitterService).setStreamingOptions(anyLong(),
				anyLong());
		long providedMessagesLimit = 3;
		String[] args = {
				"-" + TwitterApplicationImpl.MESSAGES_LIMIT_SHORT_OPTION,
				"" + providedMessagesLimit };

		twitterApplication.run(args, BIEBER);

		verify(twitterService).setStreamingOptions(
				TwitterService.DEFAULT_STREAMING_PERIOD, providedMessagesLimit);
		verify(twitterService).retrieveMessages(anyString(), anyPrintStream());
		verify(printingService).printTwitterMessages(anyMessagesByAuthor(),
				anyPrintStream());
	}

	@Test
	public void shouldRetrieveTwitterMessagesAndPrintThemWithStreamingPeriodOption()
			throws TwitterAuthenticationException, IOException, ParseException,
			InvalidCommandLineParameterException, java.text.ParseException {

		TreeMap<Author, TreeSet<Message>> dummyMessagesByAuthor = Maps
				.newTreeMap();
		doReturn(dummyMessagesByAuthor).when(twitterService).retrieveMessages(
				anyString(), anyPrintStream());
		doNothing().when(printingService).printTwitterMessages(
				anyMessagesByAuthor(), anyPrintStream());
		doNothing().when(twitterService).setStreamingOptions(anyLong(),
				anyLong());
		long providedStreamingPeriod = 30;
		String[] args = {
				"-" + TwitterApplicationImpl.STREAMING_PERIOD_SHORT_OPTION,
				"" + providedStreamingPeriod };

		twitterApplication.run(args, BIEBER);

		verify(twitterService).setStreamingOptions(providedStreamingPeriod,
				TwitterService.DEFAULT_MESSAGES_LIMIT);
		verify(twitterService).retrieveMessages(anyString(), anyPrintStream());
		verify(printingService).printTwitterMessages(anyMessagesByAuthor(),
				anyPrintStream());
	}

	@Test(expected = InvalidCommandLineParameterException.class)
	public void shouldThrowInvalidCommandLineParameterExceptionWhenInvalidMessagesLimit()
			throws TwitterAuthenticationException, IOException, ParseException,
			InvalidCommandLineParameterException, java.text.ParseException {

		String[] args = {
				"-" + TwitterApplicationImpl.MESSAGES_LIMIT_SHORT_OPTION,
				"NaN!" };

		twitterApplication.run(args, BIEBER);
	}

	private long anyLong() {
		return any(Long.class);
	}

	private Options anyOptions() {
		return any(Options.class);
	}

	@SuppressWarnings("unchecked")
	private TreeMap<Author, TreeSet<Message>> anyMessagesByAuthor() {
		return any(TreeMap.class);
	}

	private PrintStream anyPrintStream() {
		return any(PrintStream.class);
	}

}
