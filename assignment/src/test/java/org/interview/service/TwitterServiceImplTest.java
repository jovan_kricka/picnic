package org.interview.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.TreeMap;
import java.util.TreeSet;

import org.interview.BaseTest;
import org.interview.app.AppContext;
import org.interview.entity.Author;
import org.interview.entity.Message;
import org.interview.exception.TwitterAuthenticationException;
import org.interview.oauth.twitter.TwitterAuthenticator;
import org.interview.parsing.AuthorJsonParser;
import org.interview.parsing.MessageJsonParser;
import org.interview.resources.Language;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppContext.class, loader = AnnotationConfigContextLoader.class)
public class TwitterServiceImplTest extends BaseTest {

	@Mock
	private TwitterAuthenticator twitterAuthenticatorMock;
	@Autowired
	private Language language;
	@Autowired
	private AuthorJsonParser authorJsonParser;
	@Autowired
	private MessageJsonParser messageJsonParser;

	private TwitterServiceImpl twitterService;
	private TwitterServiceImpl twitterServiceSpy;

	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		twitterService = new TwitterServiceImpl(twitterAuthenticatorMock,
				language, authorJsonParser, messageJsonParser);
		twitterServiceSpy = spy(twitterService);
	}

	@Test
	public void shouldReturnMessagesByAuthorFetchedFromTwitter()
			throws TwitterAuthenticationException, IOException, ParseException {
		mockTwitterAuthenticator();

		TreeMap<Author, TreeSet<Message>> messagesByAuthors = twitterServiceSpy
				.retrieveMessages("BIEBER", System.out);

		assertMessagesByAuthors(messagesByAuthors);
	}

	@Test(expected = IOException.class)
	public void shouldThrowIOExceptionWhenTwitterStreamIsBroken()
			throws TwitterAuthenticationException, IOException, ParseException {
		mockBrokenTwitterAuthenticator();

		twitterServiceSpy.retrieveMessages("BIEBER", System.out);
	}

	@Test
	public void shouldRetrieveSomeMessagesWhenStreamingTimeoutExpires()
			throws TwitterAuthenticationException, IOException, ParseException {
		mockTwitterAuthenticator();
		twitterServiceSpy.setStreamingOptions(1, 100);

		TreeMap<Author, TreeSet<Message>> messagesByAuthors = twitterServiceSpy
				.retrieveMessages("BIEBER", System.out);

		assertTrue(!messagesByAuthors.isEmpty());
	}

	private void assertMessagesByAuthors(
			TreeMap<Author, TreeSet<Message>> messagesByAuthors) {
		for (int i = 1; i <= 100; i++) {
			Author author = buildAuthor(i);
			messagesByAuthors.get(author);
			assertTrue(messagesByAuthors.containsKey(author));
		}
	}

	private Author buildAuthor(int id) {
		Author author = Author
				.newBuilder()
				.withCreationDate(AUTHOR_CREATION_DATE)
				.withName("Jay DESPACITO")
				.withScreenName("drewvattor")
				.withUserId("" + id)
				.build();
		return author;
	}

	private void mockTwitterAuthenticator()
			throws IOException, TwitterAuthenticationException {
		InputStream inputStream = buildInputStream();
		doReturn(inputStream)
				.when(twitterServiceSpy)
				.getTwitterStatusesStream(anyString());
	}

	private void mockBrokenTwitterAuthenticator()
			throws IOException, TwitterAuthenticationException {
		InputStream inputStream = buildInputStream();
		inputStream.close();
		doReturn(inputStream)
				.when(twitterServiceSpy)
				.getTwitterStatusesStream(anyString());
	}

	private InputStream buildInputStream() throws FileNotFoundException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(
				classLoader.getResource(MESSAGE_STREAM_JSON).getFile());
		return new FileInputStream(file);
	}

}
