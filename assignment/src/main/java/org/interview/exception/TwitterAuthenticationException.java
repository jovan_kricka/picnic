package org.interview.exception;

public class TwitterAuthenticationException extends Exception {

	private static final long serialVersionUID = 1L;

	public TwitterAuthenticationException() {
	}

	public TwitterAuthenticationException(final String message) {
		super(message);
	}

	public TwitterAuthenticationException(final String message,
			final Throwable t) {
		super(message, t);
	}

	public TwitterAuthenticationException(final Throwable t) {
		super(t);
	}
}
