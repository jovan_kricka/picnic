package org.interview.service;

import java.io.IOException;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.TreeMap;
import java.util.TreeSet;

import org.interview.entity.Author;
import org.interview.entity.Message;
import org.interview.exception.TwitterAuthenticationException;

/**
 * Implementation of this interface should provide functionality of connecting
 * to Twitter API and retrieving messages.
 */
public interface TwitterService {

	static final long DEFAULT_STREAMING_PERIOD = 30_000;
	static final long DEFAULT_MESSAGES_LIMIT = 100;

	/**
	 * Retrieves messages that track on a specific string.
	 * 
	 * @param track
	 *            will be used for retrieving messages
	 * @param console
	 *            console for writing status messages
	 * @return {@link TreeMap} where key is the author, and value is a
	 *         {@link TreeSet} of messages
	 * @throws TwitterAuthenticationException
	 * @throws IOException
	 * @throws ParseException
	 */
	TreeMap<Author, TreeSet<Message>> retrieveMessages(String track,
			PrintStream console)
			throws TwitterAuthenticationException, IOException, ParseException;

	/**
	 * Configures parameters of this service that will determine what will be
	 * maximum streaming period and what will be maximum number of fetched
	 * messages.
	 * 
	 * @param streamingPeriod
	 *            maximum streaming period in milliseconds
	 * @param messagesLimit
	 *            maximum number of fetched messages
	 */
	void setStreamingOptions(long streamingPeriod, long messagesLimit);
}
