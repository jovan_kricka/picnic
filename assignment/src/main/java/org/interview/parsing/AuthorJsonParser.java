package org.interview.parsing;

import java.text.ParseException;

import org.interview.entity.Author;
import org.springframework.stereotype.Component;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Component
public class AuthorJsonParser extends BaseJsonParser {

	public Author parse(String json) throws ParseException {
		JsonParser parser = new JsonParser();
		JsonObject messageJsonObject = parser.parse(json).getAsJsonObject();
		JsonObject authorJsonObject = messageJsonObject.getAsJsonObject("user");
		Author author = Author
				.newBuilder()
				.withCreationDate(
						parseToMilliseconds(authorJsonObject.get("created_at")))
				.withName(stringify(authorJsonObject.get("name")))
				.withScreenName(stringify(authorJsonObject.get("screen_name")))
				.withUserId(stringify(authorJsonObject.get("id")))
				.build();
		return author;
	}

}
