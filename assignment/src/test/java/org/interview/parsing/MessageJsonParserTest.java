package org.interview.parsing;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;

import org.interview.BaseTest;
import org.interview.app.AppContext;
import org.interview.entity.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppContext.class, loader = AnnotationConfigContextLoader.class)
public class MessageJsonParserTest extends BaseTest {

	private static final String MESSAGE_ID = "857342713901051907";
	private static final String MESSAGE_TEXT = "Justin bieber is the best.";
	private static final long MESSAGE_CREATION_DATE = 1493241406000l;
	private static final String AUTHOR_ID = "850583816582332417";

	@Autowired
	private MessageJsonParser messageJsonParser;

	@Test
	public void shouldCorrectlyParseMessageFieldsFromJson()
			throws IOException, ParseException {
		String messageJson = getContentOfResourceFile(MESSAGE_JSON);

		Message message = messageJsonParser.parse(messageJson);

		assertEquals(message.getAuthorId(), AUTHOR_ID);
		assertEquals(message.getCreationDate(), MESSAGE_CREATION_DATE);
		assertEquals(message.getText(), MESSAGE_TEXT);
		assertEquals(message.getMessageId(), MESSAGE_ID);
	}

}
