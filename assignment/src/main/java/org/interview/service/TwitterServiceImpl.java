package org.interview.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.ParseException;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.interview.entity.Author;
import org.interview.entity.Message;
import org.interview.exception.TwitterAuthenticationException;
import org.interview.oauth.twitter.TwitterAuthenticator;
import org.interview.parsing.AuthorJsonParser;
import org.interview.parsing.MessageJsonParser;
import org.interview.resources.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

@Service
public class TwitterServiceImpl implements TwitterService {

	private static final String TWITTER_STATUSES_STREAM_URL = "https://stream.twitter.com/1.1/statuses/filter.json?track=";

	private final TwitterAuthenticator twitterAuthenticator;
	private final Language language;
	private final AuthorJsonParser authorJsonParser;
	private final MessageJsonParser messageJsonParser;

	private long timerStartedAt;
	private long streamingPeriod;
	private long messagesLimit;

	@Autowired
	public TwitterServiceImpl(TwitterAuthenticator twitterAuthenticator,
			Language language, AuthorJsonParser authorJsonParser,
			MessageJsonParser messageJsonParser) {
		this.twitterAuthenticator = twitterAuthenticator;
		this.language = language;
		this.authorJsonParser = authorJsonParser;
		this.messageJsonParser = messageJsonParser;
		this.streamingPeriod = DEFAULT_STREAMING_PERIOD;
		this.messagesLimit = DEFAULT_MESSAGES_LIMIT;
	}

	@Override
	public TreeMap<Author, TreeSet<Message>> retrieveMessages(String track,
			PrintStream console)
			throws TwitterAuthenticationException, IOException, ParseException {

		InputStream feedStream = getTwitterStatusesStream(track);
		TreeMap<Author, TreeSet<Message>> messagesByAuthor = Maps.newTreeMap();

		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(feedStream))) {

			console.println(language.fetchingStarted());
			resetTimer();

			while (shouldKeepStreaming(messagesByAuthor)) {

				String json = reader.readLine();

				Author author = authorJsonParser.parse(json);
				Message message = messageJsonParser.parse(json);

				addMessageByAuthor(messagesByAuthor, author, message);

				console.print(language.progress());
			}
		}
		console.println(language.fetchingFinished());

		return messagesByAuthor;
	}

	@Override
	public void setStreamingOptions(long streamingPeriod, long messagesLimit) {
		this.streamingPeriod = streamingPeriod;
		this.messagesLimit = messagesLimit;
	}

	private void addMessageByAuthor(
			Map<Author, TreeSet<Message>> messagesByAuthor, Author author,
			Message message) {
		if (messagesByAuthor.containsKey(author)) {
			SortedSet<Message> messages = messagesByAuthor.get(author);
			messages.add(message);
		} else {
			TreeSet<Message> messages = Sets.newTreeSet();
			messages.add(message);
			messagesByAuthor.put(author, messages);
		}
	}

	InputStream getTwitterStatusesStream(String track)
			throws TwitterAuthenticationException, IOException {

		GenericUrl url = new GenericUrl(TWITTER_STATUSES_STREAM_URL + track);
		HttpRequestFactory requestFactory = twitterAuthenticator
				.getAuthorizedHttpRequestFactory();
		HttpRequest getRequest = requestFactory.buildGetRequest(url);

		HttpResponse response = getRequest.execute();
		InputStream feedStream = response.getContent();

		return feedStream;
	}

	private void resetTimer() {
		timerStartedAt = System.currentTimeMillis();
	}

	private boolean isTimerExpired() {
		long now = System.currentTimeMillis();
		return (now - timerStartedAt) > streamingPeriod;
	}

	private boolean shouldKeepStreaming(
			TreeMap<Author, TreeSet<Message>> messages) {
		return !isTimerExpired() && messages.size() < messagesLimit;
	}

}
